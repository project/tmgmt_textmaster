TMGMT TextMaster (tmgmt_textmaster)
---------------------

TMGMT TextMaster module is a plugin for
Translation Management Tool module (tmgmt).
It uses the TextMaster (https://www.textmaster.com)
for automated translation of the content.

REQUIREMENTS
------------

This module requires TMGMT (http://drupal.org/project/tmgmt) module
to be installed.

Also you will need to enter your TextMaster credentials 
(API key and API secret).You can find them on the page 
https://www.app.textmaster.com/clients/api_info after 
registration on https://www.textmaster.com

Please be sure that your server time is correct. Wrong time
value may cause the authentication failure for TextMaster plugins (see 
https://eu.app.textmaster.com/api-documentation#authentication-signature-creation
for details).
