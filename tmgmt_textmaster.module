<?php

/**
 * @file
 * A description of what your module does.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Database;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_textmaster\TextmasterTranslatorUi;

/**
 * Implements hook_entity_operation().
 */
function tmgmt_textmaster_entity_operation(EntityInterface $entity) {

  /** @var Drupal\tmgmt\Entity\Job $entity */
  if ($entity->bundle() == 'tmgmt_job'
    && $entity->hasTranslator()
    && $entity->getTranslatorId() == 'textmaster'
  ) {
    $project_id = tmgmt_textmaster_get_project_by_job_id($entity->id());
    if (!$project_id) {
      return;
    }
    $textmaster_app_url = TextmasterTranslatorUi::getApplicationUrl($entity->getTranslator());

    $operations['view_on_tm'] = [
      'url' => Url::fromUri($textmaster_app_url . '/clients/projects/' . $project_id, [
        'attributes' => [
          'target' => '_blank',
        ],
      ]),
      'title' => t('View on TextMaster'),
      'weight' => 10,
    ];
    $mappings = $entity->getRemoteMappings();
    $job_remote_data = end($mappings);
    $auto_launch = $job_remote_data->remote_data->TemplateAutoLaunch;
    if ($entity->isState(Job::STATE_UNPROCESSED) && !$auto_launch) {
      $operations['launch'] = [
        'url' => Url::fromRoute('tmgmt_textmaster.launch', [
          'tm_job_id' => $entity->id(),
          'tm_project_id' => $project_id,
        ]),
        'title' => t('Launch'),
        'weight' => 10,
      ];
    }
    return $operations;
  }
  /** @var Drupal\tmgmt\Entity\JobItem $entity */
  if ($entity->bundle() == 'tmgmt_job_item'
    && $entity->getJob()->getTranslatorId() == 'textmaster') {
    $remote = tmgmt_textmaster_get_job_item_remote($entity);
    $module_name = $remote['module_name'];
    $document_id = $remote['document_id'];
    $project_id = $remote['project_id'];
    if (!$project_id || $module_name != 'tmgmt_textmaster') {
      return;
    }
    $textmaster_app_url = TextmasterTranslatorUi::getApplicationUrl($entity->getTranslator());
    $operations['view_on_tm'] = [
      'url' => Url::fromUri($textmaster_app_url . '/clients/projects/' . $project_id . '/documents/' . $document_id, [
        'attributes' => [
          'target' => '_blank',
        ],
      ]),
      'title' => t('View on TextMaster'),
      'weight' => 10,
    ];
    return $operations;
  }

}

/**
 * Implements hook_views_data_alter().
 */
function tmgmt_textmaster_views_data_alter(array &$data) {

  $data['tmgmt_job']['tmgmt_textmaster_price'] = [
    'title' => t('Project Price'),
    'help' => 'Displays a TextMaster price of the job.',
    'real field' => 'settings',
    'field' => [
      'title' => t('Project Price'),
      'id' => 'tmgmt_textmaster_price',
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function tmgmt_textmaster_form_tmgmt_job_abort_form_alter(&$form) {
  /** @var Drupal\tmgmt\Entity\Job $job */
  $job = \Drupal::routeMatch()->getParameter('tmgmt_job');
  if (empty($job) ||
    !$job->hasTranslator() ||
    $job->getTranslatorId() != 'textmaster'
  ) {
    return;
  }
  // Set new message to confirm Job abortion for TextMaster Jobs.
  $confirmation_message = t("This will send a request to TextMaster to abort the job. Please know that if the job has been started on TextMaster side, you won’t be paid back.");
  $form['description']['#markup'] = $confirmation_message->render();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function tmgmt_textmaster_form_views_exposed_form_alter(&$form, $form_state, $form_id) {
  $view = $form_state->get('view');

  if (!empty($view) && $view->id() == 'tmgmt_job_overview') {
    // Add warning message about word count.
    drupal_set_message(t('Please note that Drupal word count may differ from TextMaster.'), 'warning');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function tmgmt_textmaster_form_tmgmt_job_item_edit_form_alter(&$form, $form_state) {
  /** @var Drupal\tmgmt\Entity\JobItem $job_item */
  $job_item = $form_state->getFormObject()->getEntity();
  /** @var Drupal\tmgmt\Entity\Job $job */
  $job = $job_item->getJob();
  if (!$job->hasTranslator()
    || $job->getTranslatorId() != 'textmaster'
    || $job_item->bundle() != 'tmgmt_job_item'
  ) {
    return;
  }

  if (!$job_item->isState(JobItemInterface::STATE_REVIEW)
  ) {
    // Cannot ask for revision if job item is not in_review status.
    return;
  }
  // Check for TextMaster Document status.
  /** @var \Drupal\tmgmt_textmaster\Plugin\tmgmt\Translator\TextmasterTranslator $plugin */
  $plugin = $job_item->getTranslatorPlugin();
  $plugin->setTranslator($job_item->getTranslator());
  $remote = tmgmt_textmaster_get_job_item_remote($job_item);
  $document_id = $remote['document_id'];
  $project_id = $remote['project_id'];
  $tm_document_data = $plugin->getTmDocument($project_id, $document_id);
  if (empty($tm_document_data)
    || !array_key_exists('status', $tm_document_data)
  ) {
    return;
  }
  if (!$form_state->isRebuilding()) {

    // Add messages about Document status in TextMaster.
    if ($tm_document_data['status'] === 'in_review') {
      drupal_set_message(t('If content is not accepted within 7 days, it will be automatically validated by TextMaster with no possible revision request.'), 'warning');
    }
    elseif ($tm_document_data['status'] === 'incomplete') {
      drupal_set_message(t('Please note that TextMaster Document for this job item is in status "incomplete" and you wont be able to accept the translation until TextMaster author finishes his work.'), 'warning');
    }
    elseif ($tm_document_data['status'] === 'completed') {
      drupal_set_message(t('Please note that TextMaster Document for this job item is already completed.'), 'warning');
    }
  }

  // Revision message can be sent only if TextMaster document is in status
  // "in_review".
  if ($tm_document_data['status'] != 'in_review') {
    return;
  }

  // All checks passed. Add new button.
  $form['actions']['ask_revision_in_tm'] = [
    '#type' => 'submit',
    '#value' => t('Ask for revision in TextMaster'),
    '#validate' => [[TextmasterTranslatorUi::class, 'askForRevisionValidate']],
    '#ajax' => [
      'callback' => [TextmasterTranslatorUi::class, 'askForRevisionCallback'],
    ],
    '#attributes' => [
      'class' => ['ask-for-revision-button'],
    ],
    '#limit_validation_errors' => [],
    '#weight' => 21,
  ];

  // Add container for revision message.
  $form['revision_message_wrapper'] = [
    '#type' => 'container',
    '#attributes' => ['id' => 'revision-message-wrapper'],
  ];
  if ($form_state->isRebuilding()
    && $form_state->get('ask_for_revision')
  && $form_state->get('revision_message_sent') === NULL) {
    // If this is an AJAX-Request, add field for revision message.
    $form['revision_message_wrapper']['revision_message'] = [
      '#type' => 'textarea',
      '#title' => t('Revision message'),
      '#default_value' => '',
      '#rows' => 3,
      '#description' => t('Please enter revision message'),
      '#weight' => 10,
    ];
    // Submit revision message button.
    $form['revision_message_wrapper']['request_revision'] = [
      '#type' => 'submit',
      '#value' => t('Request for revision in TextMaster'),
      '#validate' => [[TextmasterTranslatorUi::class, 'sendRevisionRequestValidate']],
      '#submit' => [[TextmasterTranslatorUi::class, 'sendRevisionRequestSubmit']],
      '#ajax' => [
        'callback' => [TextmasterTranslatorUi::class, 'sendRevisionRequestCallback'],
      ],
      '#weight' => 20,
    ];
    // Hide "Ask for revision" button.
    $form['actions']['ask_revision_in_tm']['#access'] = FALSE;
  }

}

/**
 * Get TextMaster project ID (last if more than 1) by translation job id.
 *
 * @param string $job_id
 *   The ID of translation job.
 *
 * @return string|false
 *   The TextMaster project ID or FALSE.
 */
function tmgmt_textmaster_get_project_by_job_id($job_id) {
  $db = Database::getConnection();
  $result = $db->select('tmgmt_remote', 'remote')
    ->fields('remote', ['remote_identifier_2'])
    ->condition('remote_identifier_1', 'tmgmt_textmaster', '=')
    ->condition('tjid', $job_id, '=')
    ->orderBy('trid', 'DESC')
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();
  return (isset($result['remote_identifier_2'])) ? $result['remote_identifier_2'] : FALSE;
}

/**
 * Get data with 'module_name', 'project_id', 'document_id' for Job Item.
 *
 * @param \Drupal\tmgmt\JobItemInterface $job_item
 *   Job Item.
 *
 * @return array
 *   Associative array with basic remote data.
 */
function tmgmt_textmaster_get_job_item_remote(JobItemInterface $job_item) {
  // Get the mapping only for the last created Project.
  $mappings = $job_item->getRemoteMappings();
  $remote_mapping = end($mappings);
  $basic_remote_data = [
    'module_name' => $remote_mapping->remote_identifier_1->value,
    'project_id' => $remote_mapping->remote_identifier_2->value,
    'document_id' => $remote_mapping->remote_identifier_3->value,
  ];
  return $basic_remote_data;
}
